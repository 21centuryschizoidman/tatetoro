# TaTeToro

El TaTeTi toroidal es similar al ta-te-ti clasico sobre un tablero de 3 × 3, pero se
considera que las celdas estan conectadas de manera toroidal. Es decir, un jugador gana si
ubica tres piezas propias en tres celdas seguidas (en una misma fila, columna o diagonal), y
se considera que una diagonal continua del otro lado del tablero.

En estos ejemplos gana el circulo:

![alt text](res/tp1.2.2020-page-001.jpg?raw=true)

El algoritmo que determina si alguien gano el juego fue optimizado para trabajar con una complejidad temporal de O(log n), todas las demas funciones trabajan en O(1).
