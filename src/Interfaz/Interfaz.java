package Interfaz;
import codigoNegocio.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.HashMap;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import java.awt.Font;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JTextArea;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


public class Interfaz 
{
	private JFrame frmTatetoro;
	private Juego juego;
	private char jugador;
	private final Color colorFondo = new java.awt.Color(96, 111, 130);
	// Creacion de las fonts que voy a usar para mostrar las fichas del juego y diferentes mensajes.
	private final Font letraFichas = new Font("Comic Sans MS", Font.PLAIN, 99);
	private final Font letraMensajes = new Font("Comic Sans MS", Font.PLAIN, 20);
	private HashMap<String, Integer> partidasGanadas;

	/**
	 * Launch the application.
	 */
		
	public static void main(String[] args) 
	{
		EventQueue.invokeLater(new Runnable() 
		{
			public void run() 
			{
				try 
				{
					Interfaz window = new Interfaz();
					window.frmTatetoro.setVisible(true);
				} catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Interfaz() 
	{
		initialize();
	}

	public void generarTablero() {
		frmTatetoro = new JFrame();
		frmTatetoro.setTitle("TaTeToro");
		frmTatetoro.getContentPane().setBackground(colorFondo);
		frmTatetoro.setResizable(false);
		frmTatetoro.getContentPane().setForeground(Color.WHITE);
		frmTatetoro.setBounds(100, 100, 850, 550);
		frmTatetoro.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmTatetoro.getContentPane().setLayout(null);
		
		// JLabel que muestra en la ventana el tablero del juego
		JLabel tablero = new JLabel("");
		tablero.setBounds(10, 0, 531, 500);
		tablero.setIcon(new ImageIcon("res//board.png"));
		frmTatetoro.getContentPane().add(tablero);
		
		// Mensaje que muestra de quien es el turno
		JTextArea quienJuega = new JTextArea();
		quienJuega.setForeground(Color.BLACK);
		quienJuega.setBounds(568, 44, 247, 51);
		quienJuega.setFont(new Font("Franklin Gothic Medium", Font.PLAIN, 20));
		quienJuega.setLineWrap(true);
		quienJuega.setEditable(false);
		quienJuega.setForeground(colorFicha(juego.quienJuega()));
		quienJuega.setText("Ahora juega: " + juego.getNombre(juego.quienJuega()));
		quienJuega.setBackground(colorFondo);
		frmTatetoro.getContentPane().add(quienJuega);
		
		// Contador de turnos
		JTextArea cantTurnos = new JTextArea();
		cantTurnos.setBounds(568, 127, 247, 51);
		cantTurnos.setFont(new Font("Franklin Gothic Medium", Font.PLAIN, 20));
		cantTurnos.setLineWrap(true);
		cantTurnos.setEditable(false);
		cantTurnos.setText("Turno numero: " + juego.getTurno());
		cantTurnos.setBackground(colorFondo);
		frmTatetoro.getContentPane().add(cantTurnos);
		//Contador de victorias j1
		JTextArea victoriasJ1 = new JTextArea();
		victoriasJ1.setBounds(568, 200, 247, 51);
		victoriasJ1.setFont(new Font("Franklin Gothic Medium", Font.PLAIN, 20));
		victoriasJ1.setLineWrap(true);
		victoriasJ1.setEditable(false);
		String j1=juego.getNombre('x');
		victoriasJ1.setText("victorias de "+j1+": "+partidasGanadas.get(j1));
		victoriasJ1.setBackground(colorFondo);
		frmTatetoro.getContentPane().add(victoriasJ1);
		//Contador de victorias j2
		JTextArea victoriasJ2 = new JTextArea();
		victoriasJ2.setBounds(568, 300, 247, 51);
		victoriasJ2.setFont(new Font("Franklin Gothic Medium", Font.PLAIN, 20));
		victoriasJ2.setLineWrap(true);
		victoriasJ2.setEditable(false);
		String j2=juego.getNombre('o');
		victoriasJ2.setText("victorias de "+j2+": "+partidasGanadas.get(j2));
		victoriasJ2.setBackground(colorFondo);
		frmTatetoro.getContentPane().add(victoriasJ2);
		
		// Creacion del texto de la ficha A1
		JLabel posA1 = new JLabel("");
		posA1.setBounds(28, 36, 120, 114);
		posA1.setFont(letraFichas);
		frmTatetoro.getContentPane().add(posA1);
		
		// Creacion del texto de la ficha B1
		JLabel posB1 = new JLabel("");
		posB1.setBounds(190, 36, 120, 114);
		posB1.setFont(letraFichas);
		frmTatetoro.getContentPane().add(posB1);
		
		// Creacion del texto de la ficha C1
		JLabel posC1 = new JLabel("");
		posC1.setBounds(352, 36, 120, 114);
		posC1.setFont(letraFichas);
		frmTatetoro.getContentPane().add(posC1);
		
		// Creacion del texto de la ficha A2
		JLabel posA2 = new JLabel("");
		posA2.setBounds(28, 190, 120, 114);
		posA2.setFont(letraFichas);
		frmTatetoro.getContentPane().add(posA2);
		
		// Creacion del texto de la ficha B2
		JLabel posB2 = new JLabel("");
		posB2.setBounds(190, 190, 120, 114);
		posB2.setFont(letraFichas);
		frmTatetoro.getContentPane().add(posB2);
		
		// Creacion del texto de la ficha C2
		JLabel posC2 = new JLabel("");
		posC2.setBounds(352, 190, 120, 114);
		posC2.setFont(letraFichas);
		frmTatetoro.getContentPane().add(posC2);
		
		// Creacion del texto de la ficha A3
		JLabel posA3 = new JLabel("");
		posA3.setBounds(28, 344, 120, 114);
		posA3.setFont(letraFichas);
		frmTatetoro.getContentPane().add(posA3);
		
		// Creacion del texto de la ficha B3
		JLabel posB3 = new JLabel("");
		posB3.setBounds(190, 344, 120, 114);
		posB3.setFont(letraFichas);
		frmTatetoro.getContentPane().add(posB3);
		
		// Creacion del texto de la ficha C3
		JLabel posC3 = new JLabel("");
		posC3.setBounds(352, 344, 120, 114);
		posC3.setFont(letraFichas);
		frmTatetoro.getContentPane().add(posC3);
		
		// Creacion del boton y del evento para la ficha A1
		JButton jugarA1 = new JButton("");
		jugarA1.setBounds(28, 36, 120, 114);
		jugarA1.setOpaque(false);
		jugarA1.setContentAreaFilled(false);
		jugarA1.setBorderPainted(false);
		frmTatetoro.getContentPane().add(jugarA1);
		jugarA1.addActionListener(new ActionListener() 
		{
			// Cuando se clickea el boton se escribe en esa posicion la ficha indicada
			// Se actualizan los valores de quienJuega y cantTurnos y se chequea si alguien gano
			public void actionPerformed(ActionEvent e) 
			{
				if (!juego.estaOcupada('a', 1))
				{
					jugador = juego.quienJuega();
					juego.ocuparPosicion('a', 1);
					posA1.setForeground(colorFicha(jugador));
					posA1.setText(" " + jugador);
					sonidoTurno(jugador);
					eventoGano(jugador);
					actualizarValores(quienJuega, cantTurnos);
				}
			}
		});
		
		JButton jugarC3_1 = new JButton("");
		jugarC3_1.setBounds(0, 0, 120, 114);
		jugarC3_1.setOpaque(false);
		jugarC3_1.setContentAreaFilled(false);
		jugarC3_1.setBorderPainted(false);
		frmTatetoro.getContentPane().add(jugarC3_1);
		
		
		JButton jugarB1 = new JButton("");
		jugarB1.setBounds(190, 36, 120, 114);
		jugarB1.setOpaque(false);
		jugarB1.setContentAreaFilled(false);
		jugarB1.setBorderPainted(false);
		frmTatetoro.getContentPane().add(jugarB1);
		jugarB1.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				if (!juego.estaOcupada('b', 1))
				{
					jugador = juego.quienJuega();
					juego.ocuparPosicion('b', 1);
					posB1.setForeground(colorFicha(jugador));
					posB1.setText(" " + jugador);
					sonidoTurno(jugador);
					eventoGano(jugador);
					actualizarValores(quienJuega, cantTurnos);
				}
			}
		});


		JButton jugarC1 = new JButton("");
		jugarC1.setBounds(352, 36, 120, 114);
		jugarC1.setOpaque(false);
		jugarC1.setContentAreaFilled(false);
		jugarC1.setBorderPainted(false);
		frmTatetoro.getContentPane().add(jugarC1);
		jugarC1.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				if (!juego.estaOcupada('c', 1))
				{
					jugador = juego.quienJuega();
					juego.ocuparPosicion('c', 1);
					posC1.setForeground(colorFicha(jugador));
					posC1.setText(" " + jugador);
					sonidoTurno(jugador);
					eventoGano(jugador);
					actualizarValores(quienJuega, cantTurnos);
				}
			}
		});


		JButton jugarA2 = new JButton("");
		jugarA2.setBounds(28, 190, 120, 114);
		jugarA2.setOpaque(false);
		jugarA2.setContentAreaFilled(false);
		jugarA2.setBorderPainted(false);
		frmTatetoro.getContentPane().add(jugarA2);
		jugarA2.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				if (!juego.estaOcupada('a', 2))
				{
					jugador = juego.quienJuega();
					juego.ocuparPosicion('a', 2);
					posA2.setForeground(colorFicha(jugador));
					posA2.setText(" " + jugador);
					sonidoTurno(jugador);
					eventoGano(jugador);
					actualizarValores(quienJuega, cantTurnos);
				}
			}
		});


		JButton jugarB2 = new JButton("");
		jugarB2.setBounds(190, 190, 120, 114);
		jugarB2.setOpaque(false);
		jugarB2.setContentAreaFilled(false);
		jugarB2.setBorderPainted(false);
		frmTatetoro.getContentPane().add(jugarB2);
		jugarB2.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				if (!juego.estaOcupada('b', 2)) 
				{
					jugador = juego.quienJuega();
					juego.ocuparPosicion('b', 2);
					posB2.setForeground(colorFicha(jugador));
					posB2.setText(" " + jugador);
					sonidoTurno(jugador);
					eventoGano(jugador);
					actualizarValores(quienJuega, cantTurnos);
				}
			}
		});


		JButton jugarC2 = new JButton("");
		jugarC2.setBounds(352, 190, 120, 114);
		jugarC2.setOpaque(false);
		jugarC2.setContentAreaFilled(false);
		jugarC2.setBorderPainted(false);
		frmTatetoro.getContentPane().add(jugarC2);
		jugarC2.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				if (!juego.estaOcupada('c', 2)) 
				{
					jugador = juego.quienJuega();
					juego.ocuparPosicion('c', 2);
					posC2.setForeground(colorFicha(jugador));
					posC2.setText(" " + jugador);
					sonidoTurno(jugador);
					eventoGano(jugador);
					actualizarValores(quienJuega, cantTurnos);
				}
			}
		});
		
		JButton jugarC3_1_1 = new JButton("");
		jugarC3_1_1.setBounds(0, 0, 120, 114);
		jugarC3_1_1.setOpaque(false);
		jugarC3_1_1.setContentAreaFilled(false);
		jugarC3_1_1.setBorderPainted(false);
		frmTatetoro.getContentPane().add(jugarC3_1_1);


		JButton jugarA3 = new JButton("");
		jugarA3.setBounds(28, 344, 120, 114);
		jugarA3.setOpaque(false);
		jugarA3.setContentAreaFilled(false);
		jugarA3.setBorderPainted(false);
		frmTatetoro.getContentPane().add(jugarA3);
		jugarA3.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				if (!juego.estaOcupada('a', 3)) 
				{
					jugador = juego.quienJuega();
					juego.ocuparPosicion('a', 3);
					posA3.setForeground(colorFicha(jugador));
					posA3.setText(" " + jugador);
					sonidoTurno(jugador);
					eventoGano(jugador);
					actualizarValores(quienJuega, cantTurnos);
				}
			}
		});


		JButton jugarB3 = new JButton("");
		jugarB3.setBounds(190, 344, 120, 114);
		jugarB3.setOpaque(false);
		jugarB3.setContentAreaFilled(false);
		jugarB3.setBorderPainted(false);
		frmTatetoro.getContentPane().add(jugarB3);
		jugarB3.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				if (!juego.estaOcupada('b', 3)) 
				{
					jugador = juego.quienJuega();
					juego.ocuparPosicion('b', 3);
					posB3.setForeground(colorFicha(jugador));
					posB3.setText(" " + jugador);
					sonidoTurno(jugador);
					eventoGano(jugador);
					actualizarValores(quienJuega, cantTurnos);
				}
			}
		});


		JButton jugarC3 = new JButton("");
		jugarC3.setBounds(352, 344, 120, 114);
		jugarC3.setOpaque(false);
		jugarC3.setContentAreaFilled(false);
		jugarC3.setBorderPainted(false);
		frmTatetoro.getContentPane().add(jugarC3);
		jugarC3.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				if (!juego.estaOcupada('c', 3)) 
				{
					jugador = juego.quienJuega();
					juego.ocuparPosicion('c', 3);
					posC3.setForeground(colorFicha(jugador));
					posC3.setText(" " + jugador);
					sonidoTurno(jugador);
					eventoGano(jugador);
					actualizarValores(quienJuega, cantTurnos);
				}
			}
		});
	}
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() 
	{
        String nombreJ1=" ";
        String nombreJ2=" "; 
        while(nombreJ1.isBlank())
        	nombreJ1 = JOptionPane.showInputDialog("JUGADOR X, INGRESE SU NOMBRE");
        while(nombreJ2.isBlank())
        	nombreJ2 = JOptionPane.showInputDialog("JUGADOR O, INGRESE SU NOMBRE");
		juego = new Juego(nombreJ2, nombreJ1);
		partidasGanadas=new HashMap<>();
		partidasGanadas.put(nombreJ1, 0);
		partidasGanadas.put(nombreJ2, 0);
		generarTablero();
		//se crea el panel de frente
        JPanel pantallaInicio = new JPanel();
        //esto esta para que no se activen los botones del tablero debajo del panel
        pantallaInicio.addMouseListener(new MouseAdapter() {
        	@Override
        	public void mouseClicked(MouseEvent e) {
        		
        	}
        });
        pantallaInicio.setLayout(null);
        JButton playBtn = new JButton("PLAY");
        playBtn.setFont(new Font("Franklin Gothic Medium", Font.PLAIN, 15));
        playBtn.setBounds(300, 375,220,75);
        pantallaInicio.add(playBtn);
        pantallaInicio.setBackground(colorFondo);
        frmTatetoro.setGlassPane(pantallaInicio);

        frmTatetoro.getGlassPane().setVisible(true);
        playBtn.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				reproducirSonido("res//play.wav", 80);
				frmTatetoro.getGlassPane().setVisible(false);
			}
		});
		// Mensaje de bienvenidos
		JTextArea bienvenidos = new JTextArea();
		bienvenidos.setForeground(Color.BLACK);
		bienvenidos.setBounds(300, 36, 283, 38);
		bienvenidos.setFont(new Font("Franklin Gothic Medium", Font.PLAIN, 20));
		bienvenidos.setWrapStyleWord(true);
		bienvenidos.setLineWrap(true);
		bienvenidos.setEditable(false);
		bienvenidos.setText("Bienvenidos a Ta Te Toro!");
		bienvenidos.setBackground(colorFondo);
		pantallaInicio.add(bienvenidos);
		
		// Mensaje con instrucciones del juego
		JTextArea instrucciones = new JTextArea();
		instrucciones.setForeground(Color.BLACK);
		instrucciones.setBounds(300, 138, 283, 172);
		instrucciones.setFont(new Font("Franklin Gothic Medium", Font.PLAIN, 20));
		instrucciones.setWrapStyleWord(true);
		instrucciones.setLineWrap(true);
		instrucciones.setEditable(false);
		instrucciones.setText("Instrucciones: Gana el jugador que ubique tres piezas en tres celdas seguidas, pero ojo, las diagonales continuan del otro lado del tablero! Suerte!");
		instrucciones.setBackground(colorFondo);
		pantallaInicio.add(instrucciones);
		
	}
	
	private void reproducirSonido(String cancion, float porcentajeVolumen)
	 {
	   try 
	   {
		float volumen = 100-porcentajeVolumen;
	    AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File(cancion).getAbsoluteFile( ));
	    Clip clip = AudioSystem.getClip( );
	    clip.open(audioInputStream);
	    FloatControl gainControl = 
	    	    (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
	    	gainControl.setValue(-volumen); // Reduce volume by 10 decibels.
	    clip.start( );
	   }
	   catch(Exception ex)
	   {
	     System.out.println("Error with playing sound.");
	     ex.printStackTrace( );
	   }
	 }
		
	private void eventoGano(char jugador) 
	{
		if(juego.jugadorGano(jugador)) 
		{
			String nombre = juego.getNombre(jugador);
			partidasGanadas.put(nombre, partidasGanadas.get(nombre)+1);
			reproducirSonido("res//victory.wav", 70);
			int valor = JOptionPane.showConfirmDialog(null, "Felicitaciones " + juego.getNombre(jugador) + "! Desea jugar otra vez?", "Juego Finalizado", JOptionPane.YES_NO_OPTION);
			if (valor == JOptionPane.YES_OPTION) 
			{
				frmTatetoro.dispose();
				String j1=juego.getNombre('x');
				String j2=juego.getNombre('o');
				this.juego=new Juego(j2, j1);
				generarTablero();
				frmTatetoro.setVisible(true);
			}
			if (valor == JOptionPane.NO_OPTION) 
			{
				 //frame.setVisible(false);
				 frmTatetoro.dispose();
			}
		}
	}
	
	private void actualizarValores(JTextArea quienJuega, JTextArea turno)
	{
		quienJuega.setForeground(colorFicha(juego.quienJuega()));
		quienJuega.setText("Ahora juega: " + juego.getNombre(juego.quienJuega()));
		turno.setText("Turno numero: " + juego.getTurno());
	}
	
	private void sonidoTurno(char jugador) 
	{
		if (jugador == 'x') 
		{
			reproducirSonido("res//buttonX.wav", 100);
		}
		if (jugador == 'o') 
		{
			reproducirSonido("res//buttonO.wav", 100);
		}
	}
	private Color colorFicha(char jugador) {
		if(jugador=='x')
			return Color.red;
		return Color.GREEN;
	}
}
