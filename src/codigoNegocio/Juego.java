package codigoNegocio;

public class Juego 
{
	private Tablero tablero;
	private int turno; // Circulo = Par; Cruz = Impar

	public Juego(String circulo, String cruz) 
	{
		tablero = new Tablero(circulo, cruz);
		turno = 1;
	}

	public boolean ocuparPosicion(char col, int fila) 
	{
		char jugador = ' ';
		if (turno % 2 == 0)
			jugador = 'o';
		else
			jugador = 'x';
		if (tablero.ocuparTablero(jugador, col, fila)) 
		{
			turno++;
			return true;
		}
		return false;
	}

	public boolean jugadorGano(char jugador) 
	{
		return tablero.jugadorGano(jugador);
	}

	public char quienJuega() 
	{
		if (turno % 2 == 0)
			return 'o';
		return 'x';
	}

	public String getNombre(char jugador) {
		return tablero.getNombre(jugador);
	}
	
	public boolean estaOcupada(char col, int fila) 
	{
		return tablero.estaOcupada(col, fila);
	}
	
	public int getTurno() {
		return turno;
	}
	
}
