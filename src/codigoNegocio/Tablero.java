package codigoNegocio;

public class Tablero {
	private Boolean[][] tablero; // X = true, O = false, empty space = null
	private Jugador circulo;
	private Jugador cruz;

	public Tablero(String circulo, String cruz) 
	{
		tablero = new Boolean[3][3];
		this.circulo = new Jugador(circulo);
		this.cruz = new Jugador(cruz);
	}

	public boolean ocuparTablero(char jugador, char col, int fila) 
	{
		if (!(jugador == 'x' || jugador == 'o'))
			throw new IllegalArgumentException("INGRESE UN JUGADOR VALIDO");
		int c = validarCoord(col, fila);
		if (tablero[fila - 1][c] != null)
			return false;
		if (jugador == 'x') 
		{
			tablero[fila - 1][c] = true;
			cruz.gano(fila - 1, c);
			return true;
		} else 
		{
			tablero[fila - 1][c] = false;
			circulo.gano(fila - 1, c);
			return true;
		}
	}

	public boolean estaOcupada(char col, int fila) 
	{
		int c = validarCoord(col, fila);
		return (tablero[fila - 1][c] != null);
	}

	public boolean jugadorGano(char jugador) 
	{
		if (jugador == 'x')
			return cruz.gano();
		else if (jugador == 'o')
			return circulo.gano();
		else
			throw new IllegalArgumentException("JUGADOR INEXISTENTE");
	}

	private int validarCoord(char col, int fila) 
	{
		if (fila > 3 || fila < 1)
			throw new IllegalArgumentException("INGRESE UNA POSICION VALIDA");
		int c = 0;
		if (col == 'a')
			c = 0;
		else if (col == 'b')
			c = 1;
		else if (col == 'c')
			c = 2;
		else
			throw new IllegalArgumentException("INGRESE UNA POSICION VALIDA");
		return c;
	}
	
	public String getNombre(char jugador) {
		if (jugador == 'x')
			return cruz.getNombre();
		else if (jugador == 'o')
			return circulo.getNombre();
		else
			throw new IllegalArgumentException("JUGADOR INEXISTENTE");
	}

	@Override
	public String toString() 
	{
		StringBuilder s = new StringBuilder();
		for (int i = 0; i < tablero.length; i++) 
		{
			s.append("\n" + "\n");
			s.append(i + 1 + " ");
			for (int j = 0; j < tablero[i].length; j++) 
			{
				if (tablero[i][j] == null)
					s.append("######");
				else
					s.append(tablero[i][j] + " ");
			}
		}
		s.append("\n" + "   A     B     C");
		return s.toString();
	}
}
