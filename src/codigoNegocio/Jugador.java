package codigoNegocio;

public class Jugador 
{
	private String nombre;
	private int[] fila;
	private int[] col;
	private int diag;
	private int antiDiag;
	private int toro;
	private boolean gano;
	private boolean hayEsquina;

	public Jugador(String nombre) 
	{
		this.nombre = nombre;
		diag = 0;
		fila = new int[3];
		col = new int[3];
		toro = 0;
		gano = false;
		hayEsquina = false;
	}

	public void gano(int f, int c) 
	{
		int toro = 0;
		int tableroSize = 3;
		fila[f]++;
		col[c]++;
		
		if (f + c == tableroSize - 1) 
		{
			antiDiag++;
		}
		
		for(int i=0;i<tableroSize;i++) {
			if(fila[i]==col[i] && (fila[i]!=0 && col[i]!=0)) {
				toro++;
			}
		}
		
		if (f == c) 
		{
			diag++;
		}

		
		if (fila[f] == tableroSize || col[c] == tableroSize || diag == tableroSize || antiDiag == tableroSize || toro == tableroSize) 
		{
			gano = true;
		}
	}

	public boolean gano() 
	{
		return gano;
	}

	public String getNombre() 
	{
		return nombre;
	}
}
