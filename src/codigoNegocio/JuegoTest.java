package codigoNegocio;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class JuegoTest 
{
	private Juego juego;

	@Before
	public void inicializar() // Inicializacion de juego para utilizar en los tests
	{
		juego = new Juego("papito", "doom");
	}

	@Test
	public void ocuparPosicionTest() // Devuelve true si pudo ocupar la posicion
	{
		assertTrue(juego.ocuparPosicion('a', 1));
	}

	@Test
	public void ocuparPosicionOcupada() // Al intentar ocupar una posicion ya ocupada, devuelve false
	{
		juego.ocuparPosicion('b', 3);
		assertFalse(juego.ocuparPosicion('b', 3));
	}

	@Test(expected = IllegalArgumentException.class)
	public void ocuparPosColumnaExcedido() // Como la columna 'f' no existe, devuelve una excepcion
	{
		juego.ocuparPosicion('f', 1);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void ocuparPosColumnaInt() // Como las columnas son solo char, devuelve una excepcion
	{
		juego.ocuparPosicion('7', 1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void ocuparPosFilaExcedido() // Como la fila 8 no existe, devuelve una excepcion
	{
		juego.ocuparPosicion('a', 8);
	}

	@Test(expected = IllegalArgumentException.class)
	public void ocuparPosFilaChar() // Como las filas son solo int, devuelve una excepcion
	{
		juego.ocuparPosicion('b', 'a');
	}

	@Test
	public void ganaXLineaVertical() 
	{
		juego.ocuparPosicion('a', 1);
		
		juego.ocuparPosicion('c', 1);
		
		juego.ocuparPosicion('a', 2);
		
		juego.ocuparPosicion('c', 2);
		
		juego.ocuparPosicion('a', 3);
		
		
		assertTrue(juego.jugadorGano('x'));
	}
	
	@Test
	public void ganaXLineaHorizontal()
	{
		juego.ocuparPosicion('a', 1);
		
		juego.ocuparPosicion('b', 2);
		
		juego.ocuparPosicion('b', 1);
		
		juego.ocuparPosicion('c', 2);
		
		juego.ocuparPosicion('c', 1);
		
		
		assertTrue(juego.jugadorGano('x'));
	}
	
	@Test
	public void ganaXDiagonalCortada()
	{
		juego.ocuparPosicion('b', 1);
		
		juego.ocuparPosicion('a', 2);
		
		juego.ocuparPosicion('c', 2);
		
		juego.ocuparPosicion('a', 1);
		
		juego.ocuparPosicion('a', 3);
		
		
		assertTrue(juego.jugadorGano('x'));
	}
	
	@Test
	public void ganaXDiagonalCortadaAgain()
	{
		juego.ocuparPosicion('b', 1);
		
		juego.ocuparPosicion('b', 2);
		
		juego.ocuparPosicion('a', 2);
		
		juego.ocuparPosicion('a', 3);
		
		juego.ocuparPosicion('c', 3);
		
		
		assertTrue(juego.jugadorGano('x'));
	}
	
	@Test
	public void ganaOLineaVertical() 
	{
		juego.ocuparPosicion('c', 1);
		
		juego.ocuparPosicion('a', 1);
		
		juego.ocuparPosicion('c', 2);
		
		juego.ocuparPosicion('a', 2);
		
		juego.ocuparPosicion('b', 3);
		
		juego.ocuparPosicion('a', 3);
		
		
		assertTrue(juego.jugadorGano('o'));
	}
	
	@Test
	public void ganaOLineaHorizontal()
	{
		juego.ocuparPosicion('b', 2);
		
		juego.ocuparPosicion('a', 1);
		
		juego.ocuparPosicion('c', 2);
		
		juego.ocuparPosicion('b', 1);
		
		juego.ocuparPosicion('a', 3);
		
		juego.ocuparPosicion('c', 1);
		
		
		assertTrue(juego.jugadorGano('o'));
	}
	
	@Test
	public void ganaODiagonalCortada()
	{
		juego.ocuparPosicion('a', 1);
		
		juego.ocuparPosicion('b', 1);
		
		juego.ocuparPosicion('a', 2);
				
		juego.ocuparPosicion('c', 2);
		
		juego.ocuparPosicion('b', 3);
				
		juego.ocuparPosicion('a', 3);
		
		
		assertTrue(juego.jugadorGano('o'));
	}
	
	@Test
	public void ganaODiagonalCortadaAgain()
	{
		juego.ocuparPosicion('a', 3);
		
		juego.ocuparPosicion('b', 1);
		
		juego.ocuparPosicion('b', 2);
		
		juego.ocuparPosicion('a', 2);
		
		juego.ocuparPosicion('b', 3);
				
		juego.ocuparPosicion('c', 3);
		
		
		assertTrue(juego.jugadorGano('o'));
	}

}
